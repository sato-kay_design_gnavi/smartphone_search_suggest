(function($) {

/* サジェスト処理 */
/* jquery.gnsuggest.js
--------------------------------------------------------- */
/*
 var nu = navigator.userAgent.toLowerCase();
 if (nu.indexOf('iphone') == -1 && nu.indexOf('ipad') == -1 && nu.indexOf('android') == -1 && nu.indexOf('windows phone') == -1 && nu.indexOf('blackberry') == -1) {
 jquery.suggest 1.1 - 2007-08-06 * Uses code and techniques from following libraries: * 1. http://www.dyve.net/jquery/?autocomplete * 2. http://dev.jquery.com/browser/trunk/plugins/interface/iautocompleter.js * All the new stuff written by Peter Vulgaris (www.vulgarisoip.com) * Feel free to do whatever you want with this file
 jquery.gnsuggest-2.0 * 2013-06-17 extended by GOURMET NAVIGATOR INC.
 folked for smartphone site 2013.Oct
*/
	$.gnsuggest = function(input, options) {
		$ = jQuery;
		$.gnsuggest.version = function() {
			return '2.0';
		}
		var $input = $(input),//.attr('autocomplete', 'off'),
			$results = $("<ul/>"),
			$page = $(options.pageId),
			$form = $(options.wrapId),
			$clear = $input.siblings().eq(0),
			$submit = $("." + options.submitClass),
			$window = $(window),
			timeout = false,
			prevLength = 0,
			cache = [],
			cacheSize = 0,
			currentTxt = '',
			currentItems = [],
			stopCount = 0,
			focusfg = 0,
			yourtype = options.key,
			once = options.once,
			ua = navigator.userAgent,
			isAndroid2 = ua.indexOf("Linux; U; Android 2") > 0 ? true : false,
			isiPhone = ua.indexOf("iPhone") > 0 ? true : false,
			isiPhone4 = ua.indexOf("iPhone OS 4") > 0 ? true : false;
		
		/*コア関数*/
		function gnsuggest(submit) {
			var q = $input.val();
			if (submit) {
				if (q.length > 0) {
					var j = createParams({sk:q});
					$.getJSON(options.source + "?callback=?", j, function(){});
				}
			}
			if (currentTxt != q) {
				if (q.length >= options.minchars){
					var cached = checkCache(q);
					if (cached) {
						displayItems(cached['items']);
					} else {
						var j = createParams({});
						if (j != null) {
							$.getJSON(options.source + "?callback=?", j, function(json) {
								var items = json;
								displayItems(items);
								addToCache(q, items, items.length);
							});
						}
					}
				}
				else if (q.length == 0) {
					clearValue();
				}
				else {
					$results.hide();
				}
				currentTxt = q;
			}
			setTimeout(gnsuggest, options.delay);
		}

		function checkCache(q) {
			for (var i = 0; i < cache.length; i++) {
				if (cache[i]['q'] == q) {
					cache.unshift(cache.splice(i, 1)[0]);
					return cache[0];
				}
			}
			return false;
		}

		function addToCache(q, items, size) {
			while (cache.length && (cacheSize + size > options.maxCacheSize)) {
				var cached = cache.pop();
				cacheSize -= cached['size'];
			}
			cache.push({
				q: q,
				size: size,
				items: items
			});
			cacheSize += size;
		}
		
		
		$results.addClass(options.resultsClass + " js-result-" + options.category).appendTo(options.resultId).hide();

		function displayItems(items) {

			if (!items) {return;}
			
			var _i = items,
				_lists = '';
			
			for (var keys in _i) {
				switch (keys) {
					case "eki":
						_lists += '<li class="js-suggest-ttl"><b>\"' + $input.val() + '\" の駅候補</b></li>';
						for (var i=0;i<_i.eki.length;i++) {
							_lists += '<li class="js-suggest-item" data-key="' + keys + '" data-name="' + _i.eki[i].name + '" data-code="' + _i.eki[i].code + '" data-type="' + _i.eki[i].type + '" data-pref="' + _i.eki[i].pref + '" data-pref_name="' + _i.eki[i].pref_name + '" data-sc="' + _i.eki[i].sc + '"><span class="ic-history">' + _i.eki[i].name + '(' + _i.eki[i].pref_name + ')</span></li>';
						}
						break;
					case "area":
						_lists += '<li class="js-suggest-ttl"><b>\"' + $input.val() + '\" のエリア候補</b></li>';
						for (var i=0;i<_i.area.length;i++) {
							_lists += '<li class="js-suggest-item" data-key="' + keys + '" data-name="' + _i.area[i].name + '" data-code="' + _i.area[i].code + '" data-type="' + _i.area[i].type + '" data-type_detail="' + _i.area[i].type_detail + '" data-pref="' + _i.area[i].pref + '" data-pref_name="' + _i.area[i].pref_name + '" data-sc="' + _i.area[i].sc + '"><span class="ic-history">' + _i.area[i].name + '(' + _i.area[i].pref_name + ')</span></li>';
						}
						break;
					case "spot":
						_lists += '<li class="js-suggest-ttl"><b>\"' + $input.val() + '\" を含むスポット候補</b></li>';
						for (var i=0;i<_i.spot.length;i++) {
							_lists += '<li class="js-suggest-item" data-key="' + keys + '" data-name="' + _i.spot[i].name + '" data-code="' + _i.spot[i].code + '" data-type="' + _i.spot[i].type + '" data-pref="' + _i.spot[i].pref + '" data-sc="' + _i.spot[i].sc + '"><span class="ic-history">' + _i.spot[i].name + '</span></li>';
						}
						break;
					case "dish":
						_lists += '<li class="js-suggest-ttl"><b>\"' + $input.val() + '\" を含む料理ジャンル候補</b></li>';
						for (var i=0;i<_i.dish.length;i++) {
							_lists += '<li class="js-suggest-item" data-key="' + keys + '" data-name="' + _i.dish[i].name + '" + data-code="' + _i.dish[i].code + '" data-type="' + _i.dish[i].type + '" data-type_detail="' + _i.dish[i].type_detail + '" data-sc="' + _i.dish[i].sc + '"><span class="ic-history">' + _i.dish[i].name + '</span></li>';
						}
						break;
					case "kodawari":
						_lists += '<li class="js-suggest-ttl"><b>\"' + $input.val() + '\" を含むこだわり候補</b></li>';
						for (var i=0;i<_i.kodawari.length;i++) {
							_lists += '<li class="js-suggest-item" data-key="' + keys + '" data-name="' + _i.kodawari[i].name + '" data-code="' + _i.kodawari[i].code + '" data-type="' + _i.kodawari[i].type + '" data-sc="' + _i.kodawari[i].sc + '"><span class="ic-history">' + _i.kodawari[i].name + '</span></li>';
						}
						break;
				}
			}
			
			$results.html(_lists);
			if (focusfg) {$results.show();}
			
			$results.children('li').each(function() {
				if ($(this).hasClass("js-suggest-ttl")) {return;}
				
				$(this).click(function() {
					var _this = $(this),
						_key = _this.attr("data-key"),
						_name = _this.attr("data-name"),
						_hidden = "",
						_cat = _key=="eki"||_key=="area"||_key=="spot" ? "area" : "dish",
						_class = ' class="js-suggest-hidden js-hidden-' + _cat + '" ',
						_ttl = '"' + _this.text() + '"';
					
					/*sc_mobile.js：trackAjaxName_suggest_sma(a,b,c)*/
					/*a："suggest"固定（スマホ仕様）、b：選択されたワード、c：ユーザーが入力したワード*/
					if (typeof trackAjaxName_suggest_sma==="function") {
						trackAjaxName_suggest_sma("suggest", _name, $input.val());
					}
					
					$input.val(_name);
					$form.find(".js-hidden-" + _cat).remove();
					
					_hidden += '<input type="hidden" id="' + _key + '_name"' + _class + 'value="' + _name + '">';
					_hidden += '<input type="hidden" id="' + _key + '_code"' + _class + 'value="' + _this.attr("data-code") + '">';
					_hidden += '<input type="hidden" id="' + _key + '_type" class="js-suggest-hidden js-hidden-' + _cat + '" value="' + _this.attr("data-type") + '">';
					_hidden += _this.attr("data-pref") ? '<input type="hidden" id="' + _key + '_pref"' + _class + 'value="' + _this.attr("data-pref") + '">' : "";
					_hidden += _this.attr("data-pref_name") ? '<input type="hidden" id="' + _key + '_pref_name"' + _class + 'value="' + _this.attr("data-pref_name") + '">' : "";
					_hidden += _this.attr("data-type_detail") ? '<input type="hidden" id="' + _key + '_type_detail"' + _class + 'value="' + _this.attr("data-type_detail") + '">' : "";
					_hidden += '<input type="hidden" id="' + _key + '_sc"' + _class + 'value="' + _this.attr("data-sc") + '">';
					$(_hidden).appendTo($form);
					
					options.Blur();
					$results.hide();
					
					if (isAndroid2) {
						$input.removeClass("js-android2-text");
					}
					
					_this.closest("ul").find(".js-suggest-ttl").each(function() {
						$(this).text($(this).text().replace(/".*"/, _ttl));
					});
				});
			});
		}
		
		/*クッキーからエリアコード取得*/
		function getCodes(type) {
			var _cookie = jQuery.cookie("smartphone"),
				_pref = "99";
			if (_cookie) {
				_pref = _cookie.match(/setpref=[0-9]+/)[0].match(/[0-9]+/);
			}
			return _pref;
		}
		
		
		/*ストレージで使うキーの乱数ID生成*/
		function gsUID() {
			var g = '';
			if (typeof $.gncookie !== 'function') return g;
			if ($.gncookie('gsuid')) {
				g = $.gncookie('gsuid')['gsuid'];
			} else {
				g = +new Date + Math.floor(Math.random() * 1000) + Math.random().toString().substr(3, 4);
				$.gncookie('gsuid', {
					gsuid: g
				}, {
					domain: '.gnavi.co.jp',
					path: '/'
				});
			}
			return g;
		}
		
		function createParams(query) {
			var o = query;
			o.k = $input.val();
			o.s = (new Date()).getTime();
			switch (true) {
				case /^rsearch location$/.test(yourtype):
					var areacodes = getCodes('pref');
					o = $.extend(o, {
						l: (areacodes == '99' ? 'area000' : 'pref' + areacodes),
						t: 'a',
						it: 'y'
					});
					break;
				case /^rsearch cuisineKodawari$/.test(yourtype):
					var areacodes = getCodes('pref');
					o = $.extend(o, {
						l: (areacodes == '99' ? 'area000' : 'pref' + areacodes),
						t: 'k',
						it: 'y'
					});
					break;
				default:
					break;
			}
			if (o != null) o.u = gsUID();
			return o;
		}
		
		function clearValue() {
			$input.val("");
			$results.html("");
			$form.find(".js-hidden-"+options.category).remove();
		}
		
		/*トリガー*/
		$input.bind("blur", function() {
			focusfg = 0;
		});
		$input.bind("click", function() {
			if (focusfg==1) {return false;}
			options.Focus();
			$results.show();
			$(this).focus();
			$window.scrollTop(0);
			focusfg = 1;
			
			if (isAndroid2) {
				/*テキストボックスが重複して表示されるバグ対応*/
				setTimeout(function() {
					if (focusfg==1) {
						$input.addClass("js-android2-text");
					}
				}, 100);
			}
		});
		
		/* iPhoneキーボードでフォーム要素移動時 */
		if (isiPhone) {
			$input.bind("focus", function() {
				if (focusfg==1) {return false;}
				options.Focus();
				$results.show();
				$window.scrollTop(0);
				focusfg = 1;
			});
		}
		
		/*クリアボタン*/
		$clear.bind("click", function() {
			clearValue();
			if (!isAndroid2) {$input.focus();}
			focusfg = 1;
		});
		
		/*submit時のパラメータ送信*/
		$submit.bind("click", function() {
			gnsuggest(true);
		});
		
		/*実行！*/
		setTimeout(gnsuggest, options.delay);
		
		
		/*複数テキストボックスを同時に扱う処理をbind（一度だけ）*/
		if (!once) {return false;}
		
		var _$back = $(once.back),
			_$inputs = $(once.inputs),
			_$results_all = $(once.results);
		
		_$back.bind("click", function() {
			options.Blur();
			_$inputs.removeClass("js-android2-text");
		});
		
		$window.bind("load", function() {
			$("input:focus").click();
		});
		
		$window.hashchange(function() {
			var _hash = location.hash,
				_cat = _hash ? _hash.match(/search_[a-z]+/)[0].match(/[a-z]+$/) : "",
				_$results = $(".js-result-" + _cat);
			
			if (isAndroid2) {
				_$inputs.blur();
			}
			
			if (_hash.match(/search/)) {
				$page.addClass("js-suggest-view");
				$form.addClass("js-focus-" + _cat);
				_$results.show();
			}
			else {
				options.Blur();
				_$results_all.hide();
				_$inputs.removeClass("js-android2-text");
			}
		});
		
		/*Android2.3フラグ*/
		if (isAndroid2) {
			$form.addClass("js-android2");
		}
		/*iPhone4フラグ*/
		if (isiPhone4) {
			$form.addClass("js-iphone4");
		}
		/*iPhone幅超過バグ*/
		if (isiPhone) {
			$window.bind("orientationchange", function() {
				$form.hide();
				$results.hide();
				setTimeout(function() {
					$form.show();
					$results.show();
				},300);
			});
		}
	};
}(jQuery));
